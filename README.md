# **TOOL**
+ [Markdown Tables Generator](https://www.tablesgenerator.com/markdown_tables)    
+ [Copy Table in Excel and Paste as a Markdown Table](http://thisdavej.com/copy-table-in-excel-and-paste-as-a-markdown-table/)
+ [CSV To Markdown Table Generator](https://donatstudios.com/CsvToMarkdownTable)
+ [twzipcode 3+2](https://github.com/recca0120/twzipcode/tree/master/src)
+ [International Telephone Input](https://github.com/jackocnr/intl-tel-input)

# **Project** 


+ **[he_inventory_v1_DOC](https://bitbucket.org/worxpace/ak_documents/wiki/browse/he_inventory_v1_DOC)**
+ **[Worxpace_doc](https://bitbucket.org/worxpace/ak_documents/wiki/browse/Worxpace_doc)**
+ **[Bioid_doc](https://bitbucket.org/worxpace/ak_documents/wiki/browse/Bioid_doc)**
  >電腦的share資料夾
+ **[Worxpace專案開發API_doc](https://bitbucket.org/worxpace/ak_documents/wiki/Worxpace_API_doc)**


 
---

>(2018/08/03 更新)

### **[DOC](https://bitbucket.org/worxpace/ak_documents/wiki/browse/Theeye_POS_SOP_doc)**
1. Payroll Management 薪酬管理
    + [休假](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/Payroll_Management/%E4%BC%91%E5%81%87)
    + [薪資計算(員工核對薪資等)](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/Payroll_Management/%E8%96%AA%E8%B3%87%E8%A8%88%E7%AE%97(%E5%93%A1%E5%B7%A5%E6%A0%B8%E5%B0%8D%E8%96%AA%E8%B3%87%E7%AD%89))
2. 人事管理
    + [**使用系統:** Howard OFFICE 人事系統](http://hrm.theeye.tw/office_back.php)
        + [員工資料](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/Shop_Staff_Management/%E5%93%A1%E5%B7%A5%E8%B3%87%E6%96%99)
        + 員工調職
        + 員工排班
            + [排班相關問題](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/%E6%8E%92%E7%8F%AD%E7%9B%B8%E9%97%9C%E5%95%8F%E9%A1%8C)
            + [排班修改Step](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/%E6%8E%92%E7%8F%AD%E4%BF%AE%E6%94%B9)
        + 員工福利
3. 店家管理
    + 維修單與iBiz-Head Office - Shop Manager系統管理
        + [會員卡相關](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/Worxpace_%E7%B6%AD%E4%BF%AE%E5%96%AE%E8%99%95%E7%90%86Step)
        + 歷史問題列表
            + [iBiz-Head Office - Shop Manager 相關問題](https://bitbucket.org/worxpace/ak_documents/wiki/browse/Theeye_POS_SOP_doc/iBiz-Head%20Office%20-%20Shop%20Manager%20%E7%9B%B8%E9%97%9C%E5%95%8F%E9%A1%8C)
                + [會員作業->會員管理](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/iBiz-Head%20Office%20-%20Shop%20Manager%20%E7%9B%B8%E9%97%9C%E5%95%8F%E9%A1%8C/%E6%9C%83%E5%93%A1%E4%BD%9C%E6%A5%AD-%3E%E6%9C%83%E5%93%A1%E7%AE%A1%E7%90%86)
                + [系統作業->商店管理](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/iBiz-Head%20Office%20-%20Shop%20Manager%20%E7%9B%B8%E9%97%9C%E5%95%8F%E9%A1%8C/%E7%B3%BB%E7%B5%B1%E4%BD%9C%E6%A5%AD-%3E%E5%95%86%E5%BA%97%E7%AE%A1%E7%90%86)
            + [會員卡(新開卡交易、辦卡)](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/%E6%AD%B7%E5%8F%B2%E5%95%8F%E9%A1%8C%E5%88%97%E8%A1%A8_%E6%9C%83%E5%93%A1%E5%8D%A1%E6%96%B0%E9%96%8B%E5%8D%A1%E4%BA%A4%E6%98%93) 
            + [補登點數](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/%E6%AD%B7%E5%8F%B2%E5%95%8F%E9%A1%8C%E5%88%97%E8%A1%A8_%E8%A3%9C%E7%99%BB%E9%BB%9E%E6%95%B8)
            + [會員卡解鎖](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/%E6%AD%B7%E5%8F%B2%E5%95%8F%E9%A1%8C%E5%88%97%E8%A1%A8_%E6%9C%83%E5%93%A1%E5%8D%A1%E8%A7%A3%E9%8E%96)
            + [修改會員資料]
            + [行動官網](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/%E6%AD%B7%E5%8F%B2%E5%95%8F%E9%A1%8C%E5%88%97%E8%A1%A8_%E8%A1%8C%E5%8B%95%E5%AE%98%E7%B6%B2)
    + [店家POS銷售系統相關](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/POS%E7%B3%BB%E7%B5%B1)
        + [無法登入系統(需要輸入帳號密碼)](https://bitbucket.org/worxpace/ak_documents/issues/4)
        + 店家庫存問題 TODO
4. 考勤管理
    + [**使用系統:** Howard OFFICE 人事系統(連結)](http://hrm.theeye.tw/office_back.php)
        + [資料修改]
            + 人事資料有誤修改，請店家找**中查 (中部查帳中心)**修改。
        + [打卡相關問題](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/%E6%89%93%E5%8D%A1)
        + [補打卡操作Step](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/Attendance%20management/%E8%A3%9C%E6%89%93%E5%8D%A1)
        
5. Tools of Management Information System 資訊系統管理(及硬體相關)
    + 電腦維護
        + [ibiz_lite_POS系統安裝Step](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/ibiz_lite_POS%E7%B3%BB%E7%B5%B1%E5%AE%89%E8%A3%9D%20Step)
    
    + 電腦周邊硬體維護
        + [發票機](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/%E7%99%BC%E7%A5%A8%E6%A9%9F)
        + [刷條碼機](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/%E5%88%B7%E6%A2%9D%E7%A2%BC%E6%A9%9F)
        + [網路電話](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/%E7%B6%B2%E8%B7%AF%E9%9B%BB%E8%A9%B1)
        + TVBOX 電視盒
            + [TVBOX 電視盒安裝](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/TVBOX%20%E9%9B%BB%E8%A6%96%E7%9B%92%E5%AE%89%E8%A3%9D%20step)
            + [TVBOX 電視盒安裝紀錄TODO](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/TVBOX%20%E9%9B%BB%E8%A6%96%E7%9B%92%E5%AE%89%E8%A3%9D%E7%B4%80%E9%8C%84)
        + [錢箱](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/%E9%8C%A2%E7%AE%B1)
    + 網路
        + [網路斷線判斷](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/%E7%B6%B2%E8%B7%AF%E6%96%B7%E7%B7%9A%E5%88%A4%E6%96%B7)
    + 瀏覽器
        + [Google Chrome](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/%E7%80%8F%E8%A6%BD%E5%99%A8_GoogleChrome)
        + Firefox
        + Safari

    + [電視螢幕(播放廣告用的)](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/%E9%9B%BB%E8%A6%96%E8%9E%A2%E5%B9%95)



---

### **其他**
>尚未整理

1. [薪資組成](https://bitbucket.org/worxpace/ak_documents/wiki/Theeye_POS_SOP_doc/others/%E8%96%AA%E8%B3%87%E7%B5%84%E6%88%90)

